//
//  CustomTransition.swift
//  NASA Picture Of The Day
//
//  Created by Adrian Young-Hoon on 12/22/17.
//  Copyright © 2017 AdrianYH. All rights reserved.
//

import UIKit

class CustomTransition: NSObject {
    
    func showFullScreenSize(imageView: UIImageView) {
        print("Image tap")
        
        if let keyWindow = UIApplication.shared.keyWindow {
            let view = UIView(frame: keyWindow.frame)
            
            UIGraphicsBeginImageContext(view.frame.size)
            imageView.image?.draw(in: view.bounds)
            let img = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            view.backgroundColor = UIColor(patternImage: img!)
            
            view.frame = CGRect(x: 0, y: keyWindow.frame.height/4, width: keyWindow.frame.width, height: keyWindow.frame.height/2)
            
            keyWindow.addSubview(view)
            
            UIView.animate(withDuration: 0.35, delay: 0.35, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                view.frame = keyWindow.frame
                
                }, completion: { (completedAnimation) in
                
            })
        }
    }
    
    func fadeOutTextView(textView: UITextView) {
        UIView.animate(withDuration: 0.35, delay: 0, options: .curveEaseOut, animations: {
            textView.alpha = 0.0
        }, completion: nil)
    }
}
