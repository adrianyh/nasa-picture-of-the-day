//
//  Extensions.swift
//  NASA Picture Of The Day
//
//  Created by Adrian Young-Hoon on 12/21/17.
//  Copyright © 2017 AdrianYH. All rights reserved.
//

import UIKit

extension UIImageView {
    func loadImageUsingUrlString(urlString: String) {
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            if error != nil {
                print("Error: \(error)")
                return
            }
            
            DispatchQueue.main.async {
                self.image = UIImage(data: data!)
            }
            
        }).resume()
    }
}
