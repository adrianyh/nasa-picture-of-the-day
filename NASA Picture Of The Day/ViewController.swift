//
//  ViewController.swift
//  NASA Picture Of The Day
//
//  Created by Adrian Young-Hoon on 12/21/17.
//  Copyright © 2017 AdrianYH. All rights reserved.
//

import UIKit

/*
struct NASAPictureOfTheDay: Decodable {
    let copyright: String
    let date: String
    let explanation: String
    let hdurl: String
    let media_type: String
    let service_version: String
    let title: String
    let url: String
}
*/

struct NASAPictureOfTheDay {
    let copyright: String
    let date: String
    let explanation: String
    let hdurl: String
    let media_type: String
    let service_version: String
    let title: String
    let url: String
    
    init(json: [String: String]) {
        copyright = json["copyright"] ?? ""
        date = json["date"] ?? ""
        explanation = json["explanation"] ?? ""
        hdurl = json["hdurl"] ?? ""
        media_type = json["media_type"] ?? ""
        service_version = json["service_version"] ?? ""
        title = json["title"] ?? ""
        url = json["url"] ?? ""
    }
}


class ViewController: UIViewController {
    
    var nasaImageUrl = "" //"https://apod.nasa.gov/apod/image/1712/WinterSolsticeMW_Seip.jpg"
    var pictureTitle = "" //"Solstice Sun and Milky Way"

    let nasaImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "")
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let titleTextView: UITextView = {
        let textView = UITextView()
        textView.text = "Loading NASA Picture Of The Day..."
        textView.font = UIFont.boldSystemFont(ofSize: 18)
        textView.textAlignment = .center
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        setupTapGestureRecognizer()
        
        getNasaPictureOfTheDay()
        
        view.addSubview(nasaImageView)
        view.addSubview(titleTextView)
        
        setupLayout()

    }
    
    func getNasaPictureOfTheDay() {
        //Personal NASA API Key
        //let nasaUrlString = "https://api.nasa.gov/planetary/apod?api_key=PwscSNPEVxUnz8tQu3UkFnLzW1EDOq7aTuCBxHJZ"
        //Digital Factory NASA API Key
        let nasaUrlString = "https://api.nasa.gov/planetary/apod?api_key=NNKOjkoul8n1CH18TWA9gwngW1s1SmjESPjNoUFo"
        guard let url = URL(string: nasaUrlString) else { return }
        
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, err) in
            
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 && httpResponse.statusCode != 202 {
                    let alert = UIAlertController(title: "Server Response Error", message: "Server response code: \(httpResponse.statusCode) - \(HTTPURLResponse.localizedString(forStatusCode: httpResponse.statusCode))", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    print("Status Code: \(httpResponse.statusCode)")
                    return
                }
            }
            
//            guard let response = response as? HTTPURLResponse, response.statusCode != 200 else {
//                print("Status Code not 200")
//                return
//            }

            guard let data = data else { return }
            print("data: ", data)
            do {
//                let nasaPictureOfTheDay = try JSONDecoder().decode(NASAPictureOfTheDay.self, from: data)
//                print("nasaPictureOfTheDay: ", nasaPictureOfTheDay)
//                print(nasaPictureOfTheDay.title, nasaPictureOfTheDay.url)
                
                guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: String] else { return }
                let nasaPicOfTheDay = NASAPictureOfTheDay(json: json)
                print(nasaPicOfTheDay.title)
                print(nasaPicOfTheDay.url)
                self.nasaImageUrl = nasaPicOfTheDay.url
                self.pictureTitle = nasaPicOfTheDay.title
                
                if self.nasaImageUrl != "" {
                    self.setupNasaImage()
                    self.setupPictureTitle()
                }
                
            }
                
            catch let err {
                print("Error: ", err)
            }
            
        }).resume()
    }
    
    private func showAlert() {
        DispatchQueue.main.async {
        
        }
    }

    private func setupNasaImage() {
        nasaImageView.loadImageUsingUrlString(urlString: nasaImageUrl)
    }
    
    private func setupPictureTitle() {
        DispatchQueue.main.async {
            self.titleTextView.text = self.pictureTitle
        }
    }
    
    private func setupLayout() {
        nasaImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        nasaImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        nasaImageView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive = true
        nasaImageView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height/2).isActive = true
        
        titleTextView.topAnchor.constraint(equalTo: nasaImageView.bottomAnchor, constant: 20).isActive = true
        titleTextView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        titleTextView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        titleTextView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
    }
    
    func setupTapGestureRecognizer() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageViewTapped(tapGestureRecognizer:)))
        nasaImageView.isUserInteractionEnabled = true
        nasaImageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func imageViewTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        let customTransition = CustomTransition()
        customTransition.fadeOutTextView(textView: titleTextView)
        customTransition.showFullScreenSize(imageView: nasaImageView)
    }

}

