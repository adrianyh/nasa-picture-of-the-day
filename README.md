# README #

NASA APIs

### iOS App Displaying NASA Picture Of The Day ###

Instructions

* Connect to NASA picture of the day REST API and parse the JSON response payload - https://api.nasa.gov/planetary/apod?api_key=NNKOjkoul8n1CH18TWA9gwngW1s1SmjESPjNoUFo

* Display error message if server response code is neither 200 nor 202.

* Display NASA picture of the day in center of screen, maintain aspect ratio and make sure height is half of the screen height.

* Display the picture title below the picture, center align the text, word wrap if more lines are needed to render the title.

* Create custom transition, tap image to display new screen with image transitioning from original size to full screen size in the new screen and have the text fade out. Set the transition time to 0.35 sec.

* Make any part or all of the above code support unit test and provide unit test.

