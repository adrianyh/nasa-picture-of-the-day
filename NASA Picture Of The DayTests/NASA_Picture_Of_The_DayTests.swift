//
//  NASA_Picture_Of_The_DayTests.swift
//  NASA Picture Of The DayTests
//
//  Created by Adrian Young-Hoon on 12/21/17.
//  Copyright © 2017 AdrianYH. All rights reserved.
//

import XCTest
@testable import NASA_Picture_Of_The_Day

class NASA_Picture_Of_The_DayTests: XCTestCase {
    
    var sessionNasaPictureOfTheDayTest: URLSession!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        sessionNasaPictureOfTheDayTest = URLSession(configuration: URLSessionConfiguration.default)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        sessionNasaPictureOfTheDayTest = nil
        super.tearDown()
    }
    
    func testCallToNasaApi() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let url = URL(string: "https://api.nasa.gov/planetary/apod?api_key=NNKOjkoul8n1CH18TWA9gwngW1s1SmjESPjNoUFo")
        let okStatus = expectation(description: "Status Code: 200")
        let acceptedStatus = expectation(description: "Status Code: 202")
        let data = sessionNasaPictureOfTheDayTest.dataTask(with: url!, completionHandler: { (data, response, error) in
            if let error = error {
                XCTFail("Error: \(error.localizedDescription)")
                return
            }
            else if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                if statusCode == 200 {
                    okStatus.fulfill()
                }
                else if statusCode == 202 {
                    acceptedStatus.fulfill()
                }
                else {
                    XCTFail("Status Code: \(statusCode)")
                }
            }
        })
        data.resume()
        waitForExpectations(timeout: 15, handler: nil)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
            
        }
    }
    
}
